<?php


// Connect to the database using MySQLi
$host = "localhost";
$user = 'root';
$password =  '';
$database = "pixel8";

$conn = new mysqli($host, $user, $password, $database);


if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}


$first_name="Jane";
$last_name="Doe";
$middle_name="Marie";
$birthday="1990-01-01";
$address="56 Elm Street";
$sql =  "INSERT INTO employee (first_name, last_name,middle_name,birthday, address)
VALUES ('$first_name', '$last_name', '$middle_name', '$birthday', '$address')";
if ($conn->query($sql) === TRUE) {
    echo "New record created successfully";
    echo "<br>";
} else {
    echo "Error: " . $sql . "<br>" . $conn->error;
    echo "<br>";
}



// retrieve the first name, last name, and birthday of all employees in the table
$sql =  "SELECT first_name,last_name,birthday FROM employee";
$result = $conn->query($sql);


if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
        echo "First Name: " . $row["first_name"]. " - Last Name: " . $row["last_name"]. " - Birthday: " . $row["birthday"]. "<br>";
    }
} else {
    echo "0 results";
}


// retrieve the number of employees whose last name starts with the letter 'S'
$sql = " SELECT * FROM employee WHERE last_name LIKE 'S%'";
$result = $conn->query($sql);


if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
        echo "First Name: " . $row["first_name"]. " - Last Name: " . $row["last_name"]. " - Birthday: " . $row["birthday"]. "<br>";
    }
} else {
    echo "0 results";
    echo "<br>";
}


//  retrieve the first name, last name, and address of the employee with the highest ID number
$sql ="SELECT first_name, last_name,address FROM employee WHERE id=(SELECT MAX(id) FROM employee)";
$result = $conn->query($sql);


if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
        echo "First Name: " . $row["first_name"]. " - Last Name: " . $row["last_name"]. " - Address: " . $row["address"]. "<br>";
    }
} else {
    echo "0 results";
    echo "<br>";
}




$new_address="Brgy. Mapgap";
$new_last_name="Cajes";
$new_middle_name="Doe";

 
$sql = "UPDATE employee SET middle_name='$new_middle_name', last_name='$new_last_name', address='$new_address' WHERE first_name='Jane' AND last_name='Doe'";
if ($conn->query($sql) === TRUE) {
    echo "Record updated successfully";
    echo "<br>";
} else {
    echo "Error updating record: " . $conn->error;
    echo "<br>";
}






// Delete data from the employee table
$sql = "DELETE FROM employee where first_name='Jane' and last_name='cajes'";
if ($conn->query($sql) === TRUE) {
    echo "Record deleted successfully";
    echo "<br>";
} else {
    echo "Error deleting record: " . $conn->error;
    echo "<br>";
}

$conn->close();


?>
